// utility to run multiple commands in parallel

package main

import (
    "flag"
    "fmt"
    "log"
    "os"

    "bitbucket.org/GottySG36/command"
)

var (
    synopsis    = `Tool to run commands in parallel`
    uString     = `go-parallel -c [-args <ARG1,...>] [-f FILE] [-sep STR] [-sif <FILE/IMAGE>] 
                    [-s-args <ARG1,...>] [-s-path <PATH/TO/singularity>] [-t INT]`

    cmd         = flag.String("c",      "",     "Command / Path to use in parallel.")
    args        = flag.String("args",   "",     "Arguments to pass to Command `c`. Uses a special format described below.")
    file        = flag.String("f",      "",     "File containing a list of file to read inputs from. See below.")
    sep         = flag.String("sep",    "\t",   "Seperator use in `file`.")

    sif         = flag.String("sif",    "", "Path to singularity image to use, if necessary.")
    sArgs       = flag.String("s-args", "", "Arguments to pass to singularity.")
    sPath       = flag.String("s-path", "", "Singularity full path. Use if installed somewhere other than in your $PATH.")
    threads     = flag.Int("t",         1,  "Number of concurrent jobs to run.")

    prefix      = flag.String("o",   "",    "Output prefix/file path in which to write concatenated output")
    errOut      = flag.Bool("w-err",    false, "Specifies that you also wish to write out the standard error. Usefull if the program wirtes som info to stderr.")
    cat         = flag.Bool("cat",   false, `Specifies that the output should be concatenated in a single file. 
If not specified, the job Id will be appended to the output prefix.`)
    verbose     = flag.Bool("v",    false,  "Additionnal print")
    force       = flag.Bool("force", false, `Specifies that we wish to attempt to write a command's output to 
file even if there were some errors.`)
    // Could be interesting at some point to add an option to parse a single file but in chuncks of N bytes

    details     = `
This is utility is not a replacement of GNU parallel but simply another tool that solved an execution
problem in some cases. 

Its usage is pretty straight forward. You provide a command to use along with its arguments. 

Its is also designed to integrated singularity / docker containers to use instead of whats in your $PATH.
In which case, you need to install singularity yourself (see : https://sylabs.io/singularity/).

THe main way to use this tool is to provide it a command and arguments. Arguments use Go(lang)'s string 
formatting and will use the information found in the specified file to fill in the missing information. 
For instance, you wish to run cat FILE1 FILE2 or some other command that requires a lot of CPU time on 
a long list of files, you could do something like 'cat %v %v' and provide a file in which every row contains 
two files delimited by à seperator.

Example of file :
abc.txt,def.txt
abc2.txt,def2.txt
...

This would cause the program to read the file and substitute in order the flags in your arguments :

cat abc.txt def.txt
cat abc2.txt def2.txt
and so on... `
)

var Usage = func() {
    fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", synopsis)
    fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uString)

    fmt.Fprintf(flag.CommandLine.Output(), "Arguments of %s:\n", os.Args[0])
    flag.PrintDefaults()

    fmt.Printf("\nDetailed description :\n%v\n", details)
}

func main() {
    flag.Usage = Usage
    flag.Parse()

    // Init(c, a, f, sep, sif, sa string, t int) (Tasks, error)
    tasks, err := command.Init(*cmd, *args, *file, *sep, *sif, *sArgs, *sPath, *threads, *verbose)
    if err != nil {
        log.Fatalf("Error -:- Init : %v\n", err)
    }
    tasks.RunParallel()

    errs := tasks.CheckErrors()
    if errs != nil {
        if *force {
            log.Print("There were some execution errors, writing output when possible...")
        } else {
            log.Fatal("There were some execution errors, exitting...")
        }
    }

    if *cat {
        err := tasks.WriteSingleOutput(*prefix)
        if err != nil {
            log.Fatalf("Error -:- WriteSingleOutput : %v\n", err)
        }
        if *errOut {
            err := tasks.WriteSingleErrorOutput(*prefix)
            if err != nil {
                log.Fatalf("Error -:- WriteSingleErrorOutput : %v\n", err)
            }
        }
    } else {
        err := tasks.WriteMultOutput(*prefix)
        if err != nil {
            log.Fatalf("Error -:- WriteMultOutput : %v\n", err)
        }
        if *errOut {
            err := tasks.WriteMultErrorOutput(*prefix)
            if err != nil {
                log.Fatalf("Error -:- WriteMultErrorOutput : %v\n", err)
            }
        }
    }
}
